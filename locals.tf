# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

locals {
  helm_charts = [
    { project = "dyff/workflows-sink", chart = "workflows-sink", full_url = "gitlab.com/dyff/workflows-sink" },
    { project = "dyff/workflows-informer", chart = "workflows-informer", full_url = "gitlab.com/dyff/workflows-informer" },
    { project = "dyff/workflows-aggregator", chart = "workflows-aggregator", full_url = "gitlab.com/dyff/workflows-aggregator" },
    { project = "dyff/dyff-api", chart = "dyff-api", full_url = "gitlab.com/dyff/dyff-api" },
    { project = "dyff/dyff-operator", chart = "dyff-operator", full_url = "gitlab.com/dyff/dyff-operator" },
    { project = "dyff/dyff-orchestrator", chart = "dyff-orchestrator", full_url = "gitlab.com/dyff/dyff-orchestrator" },
    { project = "dyff/dyff-frontend", chart = "dyff-frontend", full_url = "gitlab.com/dyff/dyff-frontend" },
  ]

  helm_charts_map = {
    for chart in local.helm_charts : chart.project => chart
  }
}
