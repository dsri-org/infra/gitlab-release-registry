# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/54955537/terraform/state/production"
    lock_address   = "https://gitlab.com/api/v4/projects/54955537/terraform/state/production/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/54955537/terraform/state/production/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}
