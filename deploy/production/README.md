# gitlab-release-registry deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

## Requirements

| Name                                                            | Version   |
| --------------------------------------------------------------- | --------- |
| <a name="requirement_gitlab"></a> [gitlab](#requirement_gitlab) | ~> 16.8.1 |

## Providers

No providers.

## Modules

| Name                                            | Source | Version |
| ----------------------------------------------- | ------ | ------- |
| <a name="module_root"></a> [root](#module_root) | ../..  | n/a     |

## Resources

No resources.

## Inputs

No inputs.

## Outputs

| Name                                                              | Description |
| ----------------------------------------------------------------- | ----------- |
| <a name="output_charts"></a> [charts](#output_charts)             | n/a         |
| <a name="output_charts_map"></a> [charts_map](#output_charts_map) | n/a         |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- prettier-ignore-end -->
