# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

output "charts" {
  value = module.root.charts
}
output "charts_map" {
  value = module.root.charts_map
}
