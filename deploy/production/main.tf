# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

module "root" {
  source = "../.."

  environment = "production"
}
