# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "16.8.1"
  constraints = "~> 16.8.1"
  hashes = [
    "h1:DckfSLS+kvajbjF98OL8qoQJW7RfluCAQXgAVHfjZYs=",
    "zh:04d2594aac4c313d8a78c97c8f389211c122beaa80d9702f2808a1429f0e41b5",
    "zh:096f61d33bb8667757ac75f3e57f21ca90b1caca9ca8beb0a75cbeb7034024ed",
    "zh:1c0af484f14e16892e4f47dad32ebb90c029583f42a1348eb23551e963d87d9f",
    "zh:2e21e59837f8d035932d22cf276dc5712ae7aeeb2cf0aadbd4abd391a8c73b3f",
    "zh:3601e8390461794ab38751762640dfe1c3e2708c8d55b4a10a1ab41e544dba34",
    "zh:3a7c27d298fabf85eec35ce1e5bf80c66866573e726772a427c28a23077ffa81",
    "zh:3be047ed882877d7ac626272a03243344fd0168df8bc806c3d42fbb297d3b809",
    "zh:65182050b36c64a7088ec28028d3871a4525c33d3f5aaf488fdeb77e081db2bd",
    "zh:9d8fe744bbae9975564c9512146b3e105631921d41b4380fc93b5a899b7ee06d",
    "zh:b35a5f0f6b46a1161f35357ae960bc4b66f6257e8d3e99331462df7c2b719fdc",
    "zh:b778d83296a7ec28659b962e61cf4dc5ea9773f5dfd81680c8ae268c20b0418b",
    "zh:c1ff7a7b0a363ba119efb1fcd7d577e5acf6807d6c799851ee9c5fd8c7510fea",
    "zh:dbc4161ad0d58fe9b45b17e023dbf07137bba401bab3d3a9a782e37861e0ad45",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:f9b0c827d90aacdad7cb6c38b913a12469a2fe3ed20df35ade4f9d7ae84e2af2",
  ]
}
