# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

resource "gitlab_deploy_token" "deploy_token_dyff_chart" {
  for_each = local.helm_charts_map
  project  = "54750400" #project id is dyff/charts
  name     = each.value.full_url
  username = each.value.chart

  scopes = ["write_registry", "read_registry"]
}
resource "gitlab_project_variable" "helm_registry_password" {
  for_each  = local.helm_charts_map
  project   = data.gitlab_project.projects[each.key].id
  key       = "HELM_REGISTRY_PASSWORD"
  value     = gitlab_deploy_token.deploy_token_dyff_chart[each.key].token
  protected = true
  masked    = true
}
resource "gitlab_project_variable" "helm_registry_key" {
  for_each  = local.helm_charts_map
  project   = data.gitlab_project.projects[each.key].id
  key       = "HELM_REGISTRY"
  value     = "registry.gitlab.com/dyff/charts"
  protected = true
  masked    = false
}
resource "gitlab_project_variable" "helm_registry_user" {
  for_each  = local.helm_charts_map
  project   = data.gitlab_project.projects[each.key].id
  key       = "HELM_REGISTRY_USER"
  value     = each.value.chart
  protected = true
  masked    = true
}
