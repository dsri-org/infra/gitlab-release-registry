# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

data "gitlab_project" "projects" {
  for_each            = local.helm_charts_map
  path_with_namespace = each.value.project
}
