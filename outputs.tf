# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: MIT

output "charts" {
  value = local.helm_charts
}
output "charts_map" {
  value = local.helm_charts_map
}
